#!/usr/bin/python

import os
import sys
import re
import datetime
import json

sections = [
  {
    "title": "Our ReVolt Pub",
    "forums": [2, 1, 10, 8, 5, 3]
  },
  {
    "title": "Our ReVolt Brewery",
    "forums": [6, 7, 11, 12, 14]
  },
  {
    "title": "RVGL & Re-Volt v1.2",
    "forums": [13, 15, 16]
  }
]

forums = {
  1: {
    "title": "The bar", 
    "topics": []
  },
  2: {
    "title": "News",
    "topics": []
  },
  3: {
    "title": "Staff Lounge",
    "topics": []
  },
  5: {
    "title": "Outside the pub",
    "topics": []
  },
  6: {
    "title": "Re-Volt Cars",
    "topics": []
  },
  7: {
    "title": "Re-Volt Levels",
    "topics": []
  },
  8: {
    "title": "The Snug",
    "topics": []
  },
  10: {
    "title": "Online Racing",
    "topics": []
  },
  11: {
    "title": "T T T Master Class",
    "topics": []
  },
  12: {
    "title": "Development, Projects & Tools",
    "topics": []
  },
  13: {
    "title": "RVGL & Re-Volt v1.2",
    "topics": []
  },
  14: {
    "title": "Authorized Personnel Only",
    "topics": []
  },
  15: {
    "title": "Bug Reports",
    "topics": []
  },
  16: {
    "title": "Suggestions",
    "topics": []
  }
}

topics = {}

num_topics = 2359

title_re = r"<h2>(.+)</h2>"
page_re = r"<div class=\"page-number\">Page <strong>(\d+)</strong> of <strong>(\d+)</strong></div>"
forum_re = r"<p><a href=\"https://www.tapatalk.com/groups/ourrevoltpub/viewtopic\.php\?f=(\d+)\&amp;t=(\d+)\">.*</a></p>"

index_head = '''
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Our ReVolt Pub</title>
<link href="print.css" rel="stylesheet">
</head>
<body id="phpbb">
<div id="wrap" class="wrap">
<a id="top" class="top-anchor" accesskey="t"></a>
<div id="page-header">
<h1><a href="index.html">Our ReVolt Pub</a></h1>
<p>Highly informative and interactive forum for the R/C Racing Sim PC game Re-Volt by Acclaim.<br/>
<a href="https://www.tapatalk.com/groups/ourrevoltpub/">https://www.tapatalk.com/groups/ourrevoltpub/</a></p>
</div>
<div id="page-body" class="page-body">
'''

index_foot = '''
</div>
<div id="page-footer" class="page-footer">
<div class="page-number">Generated on {}</div>
</div>
</div>
</body>
</html>
'''

usage_text = '''
=== ORP Scraper ===
Usage:
scrap.py --build-index
scrap.py --login <user> <pass>
scrap.py --logout
scrap.py --scrape
'''

def get_cookies(username, password):
  wget_command = "wget --post-data='username={}\&password={}\&login=Login' \
     --save-cookies=cookies.txt --keep-session-cookies \
     https://www.tapatalk.com/groups/ourrevoltpub/ucp.php?mode=login"

  os.system(wget_command.format(username,password))

def clear_cookies():
  wget_command = "wget --cookies=on --keep-session-cookies --load-cookies=cookies.txt -O - \
      https://www.tapatalk.com/groups/ourrevoltpub/ucp.php?mode=logout"

  os.system(wget_command)

def scrape_topics():
  wget_command = "wget --cookies=on --keep-session-cookies --load-cookies=cookies.txt -O - \
      https://www.tapatalk.com/groups/ourrevoltpub/viewtopic.php?t={}\&start={}\&view=print"

  for topic in range(1,num_topics+1):
    data = os.popen(wget_command.format(topic,0)).read()
    with open("t{}.html".format(topic), "w") as f:
      f.write(data)

    if not data:
      continue

    m = re.search(page_re, data)
    num = int(m.group(2))

    for page in range(50,num*50,50):
      data = os.popen(wget_command.format(topic,page)).read()
      with open("t{}_s{}.html".format(topic,page), "w") as f:
        f.write(data)

def convert_links():
  os.system("sed -i 's/https:\/\/www\.tapatalk\.com\/groups\/static\/styles\/Tapatalk\/theme\/print\.css/print\.css/' *.html")
  os.system("sed -i 's/\.\/forum_data\/forums.ou\/ourr\/ourrevoltpub\/smilies/smilies/' *.html")
  os.system("sed -i 's/<h1>Our ReVolt Pub<\/h1>/<h1><a href=\"index.html\">Our ReVolt Pub<\/a><\/h1>/' *.html")

def build_index():
  for topic in range(1,num_topics+1):
    with open("t{}.html".format(topic), "r") as f:
      data = f.read()

    if not data:
      continue

    m = re.search(title_re, data)
    title = m.group(1)

    m = re.search(page_re, data)
    pages = int(m.group(2))

    topics[topic] = {
      "title": title,
      "pages": pages
    }

    m = re.search(forum_re, data)
    forum = int(m.group(1))

    forums[forum]["topics"].append(topic)

def write_index():
  now = datetime.datetime.now()

  with open("index.html", "w") as f:
    f.write(index_head)

    for section in sections:
      f.write("<h2>{}</h2>\n".format(section["title"]))
      for forum in section["forums"]:
        f.write("<h3><a href=\"f{}.html\">{}</a> ".format(forum,forums[forum]["title"]))
        f.write("[{}]</h3>\n".format(len(forums[forum]["topics"])))
      f.write("<br/><hr/>\n")

    f.write(index_foot.format(now.strftime("%Y-%m-%d %H:%M")))

  for forum in forums:
    with open("f{}.html".format(forum), "w") as f:
      f.write(index_head)

      f.write("<h2>{}</h2>\n".format(forums[forum]["title"]))
      for topic in forums[forum]["topics"]:
        f.write("<h3><a href=\"t{}.html\">{}</a> ".format(topic,topics[topic]["title"]))
        if topics[topic]["pages"] > 1:
          f.write("<a href=\"t{}.html\">[1]</a> ".format(topic))
          for page in range(1,topics[topic]["pages"]):
            f.write("<a href=\"t{}_s{}.html\">[{}]</a> ".format(topic,page*50,page+1))
        f.write("</h3>\n")
      f.write("<br/><hr/>\n")

      f.write(index_foot.format(now.strftime("%Y-%m-%d %H:%M")))

  with open('sections.json', 'w') as f:
    json.dump(sections, f)
  with open('forums.json', 'w') as f:
    json.dump(forums, f)
  with open('topics.json', 'w') as f:
    json.dump(topics, f)

def main():
  if len(sys.argv) >= 2:
    if sys.argv[1] == "--scrape":
      scrape_topics()
      convert_links()
      return
    elif sys.argv[1] == "--build-index":
      build_index()
      write_index()
      return
    elif sys.argv[1] == "--logout":
      clear_cookies()
      return
  if len(sys.argv) >= 4:
    if sys.argv[1] == "--login":
      get_cookies(sys.argv[2],sys.argv[3])
      return

  print(usage_text)

main()
